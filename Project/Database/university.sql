-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 20, 2013 at 12:55 PM
-- Server version: 5.5.24-log
-- PHP Version: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `university`
--

-- --------------------------------------------------------

--
-- Table structure for table `college`
--

CREATE TABLE IF NOT EXISTS `college` (
  `collegename` varchar(40) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `CSE` int(1) NOT NULL,
  `ECE` int(1) NOT NULL,
  `CIVIL` int(1) NOT NULL,
  `AUTO` char(1) COLLATE armscii8_bin DEFAULT NULL,
  `IT` char(1) COLLATE armscii8_bin DEFAULT NULL,
  `MECH` char(1) COLLATE armscii8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=armscii8 COLLATE=armscii8_bin;

--
-- Dumping data for table `college`
--

INSERT INTO `college` (`collegename`, `CSE`, `ECE`, `CIVIL`, `AUTO`, `IT`, `MECH`) VALUES
('mvsr', 1, 1, 1, '1', '0', '0'),
('CBIT', 1, 1, 1, NULL, '0', '0'),
('Stanley', 1, 1, 1, NULL, '0', '0'),
('Vasavi', 1, 1, 1, NULL, '0', '0'),
('Deccan', 1, 1, 1, NULL, '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `dept`
--

CREATE TABLE IF NOT EXISTS `dept` (
  `deptname` varchar(20) NOT NULL,
  `sub1` varchar(20) NOT NULL,
  `sub2` varchar(20) NOT NULL,
  `sub3` varchar(20) NOT NULL,
  `sub4` varchar(20) NOT NULL,
  `sub5` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dept`
--

INSERT INTO `dept` (`deptname`, `sub1`, `sub2`, `sub3`, `sub4`, `sub5`) VALUES
('CSE', 'dm', 'os', 'ai', 'irs', 'cc'),
('ECE', 'dm', 'sadfs', 'ai', 'irs', 'cc'),
('AUTO', 'a', 'b', 'c', 'd', 'e'),
('IT', 'a', 'b', 'c', 'd', 'e'),
('MECH', 'a', 'b', 'c', 'd', 'e');

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE IF NOT EXISTS `notification` (
  `noteid` varchar(20) NOT NULL,
  `uname` varchar(20) NOT NULL,
  `subject` varchar(20) NOT NULL,
  `message` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`noteid`, `uname`, `subject`, `message`) VALUES
('1', 'kapeskittu', 'DM', 'dfafdasdcavasdffdasf'),
('2', 'kapeskittu', 'DM', 'dfafdasdcavasdffdasf'),
('3', 'kapeskittu', 'DM', 'dfafdasdcavasdffdasf'),
('4', 'kapeskittu', 'os', 'abba'),
('5', 'kapeskittu', 'os', 'hii how r u??'),
('6', 'kapeskittu', '', ''),
('7', 'kapeskittu', '', ''),
('8', 'kapeskittu', '', ''),
('9', 'kapeskittu', '', ''),
('10', 'kapeskittu', '', ''),
('11', 'kapeskittu', '', ''),
('12', 'kapeskittu', '', ''),
('13', 'kapeskittu', '', ''),
('14', 'kapeskittu', '', ''),
('15', 'kapeskittu', '', ''),
('16', 'kapeskittu', '', ''),
('17', 'kapeskittu', '', ''),
('18', 'kapeskittu', 'dm', 'sadfasd'),
('19', 'cc', 'dm', 'hyderabad sunrisers ');

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE IF NOT EXISTS `profile` (
  `name` varchar(11) NOT NULL,
  `uname` varchar(11) NOT NULL,
  `pass` varchar(11) NOT NULL,
  `usertype` int(11) NOT NULL,
  `collegename` varchar(11) NOT NULL,
  `dept` varchar(11) NOT NULL,
  `sub1` int(11) NOT NULL,
  `sub2` int(11) NOT NULL,
  `sub3` int(11) NOT NULL,
  `sub4` int(11) NOT NULL,
  `sub5` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile`
--

INSERT INTO `profile` (`name`, `uname`, `pass`, `usertype`, `collegename`, `dept`, `sub1`, `sub2`, `sub3`, `sub4`, `sub5`) VALUES
('admin', 'admin', 'admin', 3, 'admin', 'admin', 0, 0, 0, 0, 0),
('krishna pra', 'kapeskittu', 'happydays', 0, 'mvsr', 'CSE', 1, 1, 1, 1, 0),
('Sai Chaitan', 'cc', 'cc', 1, 'mvsr', 'CSE', 1, 1, 1, 0, 0),
('Paramesh Od', 'paramesh', 'oddepally', 0, 'mvsr', 'CSE', 1, 1, 1, 1, 0),
('hemanjeni k', 'hema', 'kundem', 0, 'mvsr', 'CSE', 1, 1, 1, 1, 0),
('chiranjeevi', 'cbit', 'cbit', 0, 'CBIT', 'CSE', 1, 1, 1, 1, 1),
('charana kum', 'charana', 'kumari', 1, 'mvsr', 'CSE', 1, 1, 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `reply`
--

CREATE TABLE IF NOT EXISTS `reply` (
  `noteid` int(11) NOT NULL,
  `senderid` varchar(20) NOT NULL,
  `reply` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reply`
--

INSERT INTO `reply` (`noteid`, `senderid`, `reply`) VALUES
(5, 'cc', ''),
(5, 'cc', ''),
(5, 'cc', 'here again'),
(5, 'cc', ''),
(5, 'cc', ''),
(5, 'cc', ''),
(5, 'cc', ''),
(5, 'cc', ''),
(1, 'cc', 'kittu'),
(19, 'kapeskittu', 'arey vedava reply osthe cheppu'),
(19, 'kapeskittu', 'arey vedava reply osthe cheppu'),
(1, 'kapeskittu', 'ayayyo'),
(18, 'kapeskittu', 'ippudu chudu');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
